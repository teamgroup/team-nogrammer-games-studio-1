﻿/// <summary>
/// Main menu.
/// It will be attached to main camera.
/// </summary>
using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public Sprite onHover;

	private Sprite original;
	private SpriteRenderer sp;

	void Start(){

		sp = GetComponent<SpriteRenderer> ();
		original = sp.sprite;
	}

	void OnMouseDown(){
		Application.LoadLevel("test");
	}

	void OnMouseOver(){
		sp.sprite = onHover;
	}

	void OnMouseExit(){
		sp.sprite = original;
	}
}
