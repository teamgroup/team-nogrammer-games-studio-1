﻿using UnityEngine;
using System.Collections;

public class Fade : MonoBehaviour {

	public bool isFading = true;

	public GUIStyle blackStyle;

	private float tintAlpha = 1;

	// Use this for initialization
	void OnGUI() {
		if (isFading) {
			GUI.color = new Color(0,0,0,tintAlpha);
			GUI.Box (new Rect (-10, -10, Screen.width + 20, Screen.height + 20), "", blackStyle);
			tintAlpha -= -0.001f;
		}
	}

}
