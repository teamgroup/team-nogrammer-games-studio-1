﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour {
	bool diary = false;
	bool kidnapmore = false;

	public void SetDiary (bool value){
		diary = value;
	}
	public bool GetDiary (){
		return diary;
	}

	public void Setkidnapmore (bool value){
		kidnapmore = value;
	}
	public bool Getkidnapmore (){
		return kidnapmore;
	}
}
