﻿using UnityEngine;
using System.Collections;

public class KidnapContinue : MonoBehaviour {

	public Animator anim;
	public Animator animtwo;

	void Awake()
	{
		// Setting up the reference.
		Animator anim = GetComponent<Animator>();
		Animator animtwo = GetComponent<Animator>();
	}

	void OnTriggerEnter2D (Collider2D collider){
		if (collider.gameObject.tag == "Player") {
			if (collider.gameObject.GetComponent<Inventory> ().Getkidnapmore ()) {
				// Sets the value
				anim.SetBool("Kidnapmore", true);
				animtwo.SetBool ("Morekidnap", true);
				// Gets the value
			}
			
		}
	}
}
