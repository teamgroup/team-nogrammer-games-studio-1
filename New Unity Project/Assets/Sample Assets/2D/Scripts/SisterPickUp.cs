﻿using UnityEngine;
using System.Collections;

public class SisterPickUp : MonoBehaviour {

	public GUIText referencetotext;

	void OnTriggerEnter2D (Collider2D collider){
				//Debug.Log ("trigger");
				if (collider.gameObject.tag == "Player") {
						//Debug.Log ("if");
						if (collider.gameObject.GetComponent<Inventory> ().GetDiary ()) {
								//Debug.Log ("there");
								//Do thing that is appropriate for getting diary from strange mouse
								referencetotext.text = "Oh! There it is!";


								//collider.gameObject.GetComponent<Inventory> ().SetDiary (false);
						} else {
								//Do thing that is appropriate for not getting diary from strange mouse
								//Debug.Log ("not there");
								referencetotext.text = "I think I saw my diary in a tree back there, but I'm afraid of heights";
						}
				}
	}
	void OnTriggerExit2D (Collider2D collider){
		referencetotext.text = "";
		}
	}
